# gitlab-ci-helpers

Gitlab CI scripts that implement commonly used functionality.

## Getting started


Import scripts into the `.gitlab-ci.yml` file as needed:

```yaml
include:
  - remote: "https://gitlab.com/winterberry/gitlab-ci-helpers/-/raw/main/helpers.yml"

# some scripts will require configuration variables to be set
variables:
  TEMPLATE_INPUT_VAR_1: value_1
  TEMPLATE_INPUT_VAR_2: value_2

example-step:
  extends:
    - .template
  variables:
    EXAMPLE_INPUT_VAR_1: value_1
    EXAMPLE_INPUT_VAR_2: value_2
  script:
    - !reference [.example, script]
```